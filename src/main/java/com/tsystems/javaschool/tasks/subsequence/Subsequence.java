package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here

        /*
         *  Метод проверяет входящие аргументы на null, берёт первый элемент первого списка
         *  и ищет соответствие во втором списке. Если соответствие найдено - инкрементирует
         *  количество совпадений, сохраняет индекс места совпадения и продолжает поиск следующего
         *  элемента со следующего индекса. Если на выходе кол-во совпадений == кол-ву элементов
         *  первого списка - получаем true, иначе - false.
         */

        if (x == null || y == null) throw new IllegalArgumentException();

        int numberOfMatches = 0;
        int index = 0;

        for (int i = 0; i < x.size(); i++) {
            for (int j = index; j < y.size(); j++) {
                if (x.get(i).toString().equals(y.get(j).toString())) {
                    numberOfMatches++;
                    index = ++j;
                    break;
                }
            }
        }

        return numberOfMatches == x.size();
    }
}