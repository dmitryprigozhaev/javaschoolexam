package com.tsystems.javaschool.tasks.pyramid;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        /*
         *  Метод выполняет проверку списка на наличие null-значений, получает (если возможно)
         *  кол-во строк в потенциальной пирамиде, вычисляет кол-во колонок (символов) в строке.
         *  На основании полученных данных создает пирамиду, заполняя её с первой строки значениями
         *  исходного списка по формуле:
         *  индекс первой вставки = [кол-во строк пирамиды - номер текущей строки];
         *  количество заполнений в строке = [номер текущей строки];
         *  индекс следующей вставки = [индекс первой вставки + 2].
         */

        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();

        int row = getPyramidRow(inputNumbers);
        int column = 2 * row - 1;

        Collections.sort(inputNumbers);

        int[][] pyramid = new int[row][column];

        int value = 0;
        for (int i = 0; i < row; i++) {
            int index = row - (i + 1);
            for (int j = 0; j < i + 1; j++) {
                pyramid[i][index] = inputNumbers.get(value++);
                index += 2;
            }
            index = row - (i + 1);
        }

        return pyramid;
    }

    private int getPyramidRow(List<Integer> inputNumbers) {

        /*
         *   Метод принимает на вход исходный список и проверяет возможность построения пирамиды,
         *   исходя из его длины, и, если возможно, возвращает количество строк (высоту) пирамиды.
         *   Опытным путём установлено, что для типа Integer максимальное допустимое значение для
         *   построения пирамиды = 2_147_450_880.
         */

        boolean exist = false;
        int row = 1;

        for (int i = 1; i <= 2147450880; i += row) {
            if (inputNumbers.size() < i) break;
            if (inputNumbers.size() == i) {
                exist = true;
                break;
            }
            row++;
        }
        if (!exist) throw new CannotBuildPyramidException();

        return row;
    }
}
