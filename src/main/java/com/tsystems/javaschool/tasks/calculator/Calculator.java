package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        // TODO: Implement the logic here
        ArrayList<String> tokens = checkAndPreparing(statement);
        if (tokens == null) return null;
        Double result = calculation(tokens);
        if (result == null) return null;
        return rounding(result);
    }

    private ArrayList<String> checkAndPreparing(String statement) {

        /*
         *  Проверка строки на отсутствие недопустимых символов и проверка наличия
         *  недопустимой логики. Возвращает подготовленный ArrayList<String>, где каждый
         *  элемент является токеном (числом, знаком операции или одиночной скобкой).
         */

        if (statement == null) return null;

        Pattern pattern = Pattern.compile("[0-9 +\\-*./()]+");
        Matcher matcher = pattern.matcher(statement);
        if (!matcher.matches()) return null;

        final String NUMBERS = "1234567890";
        final String OPERATORS = "+-*/";

        statement = statement.replace(" ", "").replace("(-", "(0-");
        if (statement.charAt(0) == '-') statement = "0" + statement;

        ArrayList<String> tokens = new ArrayList<>();

        int checkBrackets = 0;
        StringBuilder temp = new StringBuilder();
        for (int i = 0; i < statement.length() - 1; i++) {
            if (OPERATORS.contains(String.valueOf(statement.charAt(i))) &&
                    !NUMBERS.contains(String.valueOf(statement.charAt(i + 1))) &&
                    statement.charAt(i + 1) != '(') return null;
            else if (statement.charAt(i) == '.' && !NUMBERS.contains(String.valueOf(statement.charAt(i + 1)))) return null;
            else if (NUMBERS.contains(String.valueOf(statement.charAt(i))) &&
                    statement.charAt(i + 1) == '(') return null;
            else if (statement.charAt(i) == '(') checkBrackets++;
            else if (statement.charAt(i) == ')') checkBrackets--;

            if (OPERATORS.contains(String.valueOf(statement.charAt(i)))) {
                tokens.add(String.valueOf(statement.charAt(i)));
            } else if (NUMBERS.contains(String.valueOf(statement.charAt(i))) &&
                    !NUMBERS.contains(String.valueOf(statement.charAt(i + 1))) &&
                    statement.charAt(i + 1) != '.') {
                temp.append(String.valueOf(statement.charAt(i)));
                tokens.add(temp.toString());
                temp.setLength(0);
            } else if (NUMBERS.contains(String.valueOf(statement.charAt(i))) &&
                    statement.charAt(i + 1) == '.') {
                temp.append(statement.charAt(i));
            } else if (NUMBERS.contains(String.valueOf(statement.charAt(i))) &&
                    NUMBERS.contains(String.valueOf(statement.charAt(i + 1)))) {
                temp.append(String.valueOf(statement.charAt(i)));
            } else if (statement.charAt(i) == '.') {
                temp.append(statement.charAt(i));
            } else if (statement.charAt(i) == '(' || statement.charAt(i) == ')')
                tokens.add(String.valueOf(statement.charAt(i)));

            if (i == statement.length() - 2) {
                if (!NUMBERS.contains(String.valueOf(statement.charAt(i + 1))) &&
                        statement.charAt(i + 1) != ')') {
                    return null;
                } else if (NUMBERS.contains(String.valueOf(statement.charAt(i + 1))) &&
                        temp.length() == 0) {
                    tokens.add(String.valueOf(statement.charAt(i + 1)));
                } else if (NUMBERS.contains(String.valueOf(statement.charAt(i + 1))) &&
                        temp.length() != 0) {
                    temp.append(statement.charAt(i + 1));
                    tokens.add(temp.toString());
                    temp.setLength(0);
                } else if (statement.charAt(i + 1) == ')') {
                    tokens.add(String.valueOf(statement.charAt(i + 1)));
                    checkBrackets--;
                } else return null;
            }
        }
        if (checkBrackets != 0) return null;
        return tokens;
    }

    private Double calculation(ArrayList<String> tokens) {

        /*
         *  Метод для вычисления выражения, подготовленного методом checkAndPreparing.
         *  Применяется алгоритм обратной польской записи. Возвращает неокруглённый
         *  результат вычислений.
         */

        final HashMap<Character, Integer> OPERATORS = new HashMap<>(); // знаки операций с приоритетами
        OPERATORS.put('+', 1);
        OPERATORS.put('-', 1);
        OPERATORS.put('*', 2);
        OPERATORS.put('/', 2);

        try {
            Stack<Double> stackOfNumbers = new Stack<>();
            Stack<String> stackOfOperators = new Stack<>();

            while (!tokens.isEmpty()) {
                String token = tokens.remove(0);
                try {
                    stackOfNumbers.push(Double.parseDouble(token));
                } catch (NumberFormatException e) {
                    if (OPERATORS.containsKey(token.charAt(0))) {
                        if (stackOfOperators.empty() || stackOfOperators.peek().equals("(")) {
                            stackOfOperators.push(token); // если стек пустой - добавляем
                        } else if (OPERATORS.get(stackOfOperators.peek().charAt(0)) <
                                OPERATORS.get(token.charAt(0))) {
                            stackOfOperators.push(token);
                        } else {
                            while (!stackOfOperators.empty() &&
                                    OPERATORS.get(stackOfOperators.peek().charAt(0)) >=
                                            (OPERATORS.get(token.charAt(0)))) {
                                stackOfNumbers.push(interimCalculations(stackOfNumbers.pop(),
                                        stackOfNumbers.pop(), stackOfOperators.pop()));
                                if (!stackOfOperators.empty() && stackOfOperators.peek().equals("(")) break;
                            }
                            stackOfOperators.push(token);
                        }
                    } else if (token.equals("(")) {
                        stackOfOperators.push(token);
                    } else if (token.equals(")")) {
                        String s = "";
                        while (!(s = stackOfOperators.pop()).equals("(")) {
                            stackOfNumbers.push(interimCalculations(stackOfNumbers.pop(), stackOfNumbers.pop(), s));
                        }
                    }
                }
                if (tokens.isEmpty()) {
                    while (!stackOfOperators.empty()) {
                        stackOfNumbers.push(interimCalculations(stackOfNumbers.pop(),
                                stackOfNumbers.pop(), stackOfOperators.pop()));
                    }
                }
            }
            return stackOfNumbers.pop();
        } catch (Exception e) {
            return null;
        }
    }

    private Double interimCalculations(Double d1, Double d2, String operation) {
        /*
         *  Простейшие промежуточные вычисления с проверкой деления на ноль.
         */
        switch (operation) {
            case "+":
                return d2 + d1;
            case "-":
                return d2 - d1;
            case "*":
                return d2 * d1;
            case "/":
                return d1 == 0 ? null : d2 / d1;
        }
        return null;
    }

    private String rounding(Double d) {
        /*
         *  Округление результата до 4 значащих цифр.
         *  Возвращает конечный результат.
         */
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
        DecimalFormat df = (DecimalFormat) nf;
        df.applyLocalizedPattern("#.####");
        return df.format(d);
    }
}